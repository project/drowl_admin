---
title: 'Entity Display Configuration'
isDeveloperContent: TRUE
---

## Field

### Fences

#### Label

Add **colon** to the field label by adding 'field__label--colon' to the field label wrapper.

#### Inline Field Items

Set class 'field-items--inline' on the field items wrapper to show field items **next each other**.

To add a **comma** between the field items you need to use 'field-items--inline-comma' instead.

#### Show Label and Value in seperate columns

Set class 'field--label-column' on the outer field wrapper. Ensure that all other wrappers are enabled.
