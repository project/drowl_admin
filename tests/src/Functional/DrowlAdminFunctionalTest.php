<?php

namespace Drupal\Tests\drowl_admin\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group drowl_admin
 */
class DrowlAdminFunctionalTest extends BrowserTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_admin',
    'test_page_test',
    'admin_toolbar',
    'layout_builder',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-drowl-admin');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests to see if the toolbar libraries are loaded on any page with toolbar.
   */
  public function testToolbarLibrariesAvailable() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
    // These are added on every page with admin toolbar on regular pages:
    $session->elementExists('css', 'head > link[href*="drowl_admin.variables"]');
    $session->elementExists('css', 'head > link[href*="drowl_admin.toolbar_fixes"]');
  }

  /**
   * Tests to see if the toolbar libraries are loaded on admin routes with toolbar.
   */
  public function testAdminRouteToolbarLibrariesAvailable() {
    $session = $this->assertSession();
    $this->drupalGet('/admin/config');
    $session->statusCodeEquals(200);
    // These are added on every page with admin toolbar on admin pages:
    $session->elementExists('css', 'head > link[href*="drowl_admin.variables"]');
    $session->elementExists('css', 'head > link[href*="drowl_admin.toolbar_fixes"]');
  }

  /**
   * Tests to see if the admin route libraries are loaded on admin routes.
   */
  public function testAdminRouteLibrariesAvailable() {
    $session = $this->assertSession();
    $this->drupalGet('/admin/config');
    $session->statusCodeEquals(200);

    // This is only added on admin routes. We're on an admin route:
    $session->elementExists('css', 'head > link[href*="drowl_admin.ckeditor_tweaks"]');
  }

}
