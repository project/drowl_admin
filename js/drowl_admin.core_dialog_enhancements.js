(function ($, Drupal) {
  Drupal.behaviors.drowl_admin_core_dialog_enhancements = {
    attach: function (context, settings) {
      // Add body classes if a modal is currently opened or not.
      // We use them in drowl_admin.theme_overrides.gin.scss to block
      // disturbing body scrolling while a modal is opened.
      if ($(".webform-ajax-form-wrapper").length == 0) {
        // Exclude webform from this, they do weird things we wont support here.
        $(once("drowl-admin-core-dialog-enhancements", window))
          .on("dialog:beforecreate", function () {
            $("body:first").addClass("drowl-admin--core-dialog-open");
          })
          .on("dialog:aftercreate", function () {
            $("body:first").addClass("drowl-admin--core-dialog-open");
          })
          .on("dialog:beforeclose", function () {
            $("body:first").removeClass("drowl-admin--core-dialog-open");
          })
          .on("dialog:afterclose", function () {
            $("body:first").removeClass("drowl-admin--core-dialog-open");
          });
      }
    },
  };
})(jQuery, Drupal);
