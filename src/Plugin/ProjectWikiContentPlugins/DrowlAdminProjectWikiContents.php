<?php

namespace Drupal\drowl_admin\Plugin\ProjectWikiContentPlugins;

use Drupal\project_wiki_markdown_content\Plugin\ProjectWikiMarkdownContentPluginBase;

/**
 * The plugin that provides the Markdown Entries to the Project Wiki.
 *
 * @ProjectWikiContent(
 *   id = "drowl_admin",
 * )
 */
class DrowlAdminProjectWikiContents extends ProjectWikiMarkdownContentPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getEntriesDirectoryPath() {
    $module_path = $this->moduleHandler->getModule('drowl_admin')->getPath();
    return $module_path . "/docs/project_wiki_markdown_content";
  }

}
